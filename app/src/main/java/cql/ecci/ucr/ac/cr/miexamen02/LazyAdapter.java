package cql.ecci.ucr.ac.cr.miexamen02;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class LazyAdapter extends BaseAdapter {

    private List<Tabletop> mData;
    private Context mContext;

    /**
     * Constructor
     * @param data que es la lista de datos que se va a dibujar al usuario
     * @param context que es el contexto de la actividad que usa el adapter
     */
    public LazyAdapter(List<Tabletop> data, Context context) {
        mData = data;
        mContext = context;
    }


    /**
     * Método que retorna el tamaño de la lista de Tabletop
     * @return el tamaño de la lista de Tabletop pasada al adapter
     */
    public int getCount() {
        if (mData.size() > 0)
            return mData.size();
        else
            return 0;
    }

    /**
     * Método que retorna un item de la lista
     * @param position que es la posición del tabletop
     * @return la instancia del Tabletop
     */
    public Object getItem(int position) {
        return mData.get(position);
    }

    /**
     * Método que retorna la posición donde se encuentra el Tabletop en la lista
     * @param position que es la posicion que se va a devolver
     * @return position
     */
    public long getItemId(int position) {
        return position;
    }


    public View getView(int position, View convertView, ViewGroup parent) { //

        // Se crea la vista a desplegar
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = null;

        Tabletop mTabletop = mData.get(position);

        // Se traen los componentes de list_row para ir desplegando cada dato del Tabletop
        rowView = inflater.inflate(R.layout.list_row, parent, false); //
        TextView nombre = rowView.findViewById(R.id.nombre);
        TextView publisher = rowView.findViewById(R.id.publisher);

        // Se le pasan los datos a los componentes
        publisher.setText(mTabletop.getPublisher());
        nombre.setText(mTabletop.getNombre());

        return rowView;
    }
}
