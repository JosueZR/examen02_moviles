package cql.ecci.ucr.ac.cr.miexamen02;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cql.ecci.ucr.ac.cr.miexamen02.Interfaces.MainActivityPresenter;
import cql.ecci.ucr.ac.cr.miexamen02.Interfaces.MainActivityView;

public class MainActivity extends AppCompatActivity implements
        MainActivityView, AdapterView.OnItemClickListener {

    private ListView mListView;
    public static ProgressDialog mDialog;

    private List<Tabletop> list;

    private MainActivityPresenter mMainActivityPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mListView = findViewById(R.id.list);
        mListView.setOnItemClickListener(this);

        // Se setean las propiedades de la barra de progreso
        mDialog = new ProgressDialog(this);
        mDialog.setMessage("Trayendo juegos...");
        mDialog.setTitle("Progreso:");
        mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mDialog.setCancelable(false);
        mDialog.setMax(100);
        mDialog.show();

        // Llamada al Presenter
        mMainActivityPresenter = new MainActivityPresenterImpl(this, getApplicationContext());
    }


    @Override protected void onResume() {
        super.onResume();
        mMainActivityPresenter.onResume();
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override protected void onDestroy() {
        mMainActivityPresenter.onDestroy();
        super.onDestroy();
    }

    // Mostrar el progreso en la UI del avance de la tarea a realizar
    @Override public void showProgress() {
        mListView.setVisibility(View.INVISIBLE);
    }

    // Esconder el indicador de progreso de la UI
    @Override public void hideProgress() {
        mListView.setVisibility(View.VISIBLE);
    }

    // Mostrar los items de la lista en la UI
    // Con la lista de items muestra la lista
    @Override public void setItems(List<Tabletop> items) {

        if (items != null)
        {
            // Se instancia el adaptador
            LazyAdapter mAdapter = new LazyAdapter(items,this);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(this);
            this.list = new ArrayList<>(items);
        }
    }

    // Mostrar mensaje en la UI
    @Override public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    // Evento al dar clic en la lista
    @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        //mMainActivityPresenter.onItemClicked(position);

        Tabletop item = this.list.get(position);
        // Se crea el nuevo intent
        Intent i= new Intent(this, DetailsTabletopActivity.class);
        i.putExtra("tabletop",item);
        startActivity(i);
    }

    // Método utilizado para retornar la barra de progreso
    public static ProgressDialog getDialog() {
        return mDialog;
    }
}