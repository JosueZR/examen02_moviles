package cql.ecci.ucr.ac.cr.miexamen02;

import android.content.Context;
import android.os.Handler;

import java.util.List;

import cql.ecci.ucr.ac.cr.miexamen02.Exceptions.CantRetrieveItemsException;
import cql.ecci.ucr.ac.cr.miexamen02.Interfaces.ItemsRepository;
import cql.ecci.ucr.ac.cr.miexamen02.Interfaces.TabletopInteractor;

public class TabletopInteractorImpl implements TabletopInteractor {

    private ItemsRepository mItemsRepository;

    @Override
    public void getItems(final OnFinishedListener listener, final Context context) {

        // Enviamos el hilo de ejecucion sin delay en este caso
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {

                List<Tabletop> items = null;
                mItemsRepository = new ItemsRepositoryImpl();

                try {
                    // Se obtienen los items
                    items = mItemsRepository.obtainItems(context, listener);

                } catch (CantRetrieveItemsException e) {
                    e.printStackTrace();
                }

                // Al finalizar retornamos los items
                //listener.onFinished(items);

            }
        },0);

    }
}
