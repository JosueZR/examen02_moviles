package cql.ecci.ucr.ac.cr.miexamen02.Exceptions;

public class DataItemsException extends Exception {

    public DataItemsException(String msg) {
        super(msg);
    }
}
