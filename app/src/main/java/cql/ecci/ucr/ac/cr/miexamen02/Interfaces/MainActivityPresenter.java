package cql.ecci.ucr.ac.cr.miexamen02.Interfaces;

public interface MainActivityPresenter {

    // Resumir
    void onResume();

    // Evento cuando se hace clic en la lista de elementos
    void onItemClicked(int position);

    // Destruir
    void onDestroy();
}
