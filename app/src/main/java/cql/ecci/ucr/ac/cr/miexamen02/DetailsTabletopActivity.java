package cql.ecci.ucr.ac.cr.miexamen02;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.widget.TextView;

public class DetailsTabletopActivity extends AppCompatActivity {

    // Se define el tabletop a recibir
    Tabletop tabletopRecibido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_tabletop);

        // Se obtiene el objeto desde la actividad principal
        tabletopRecibido = getIntent().getParcelableExtra("tabletop");

        // Se despliega un mensaje en la actividad
        TextView textView1 = findViewById(R.id.textViewActivity);
        textView1.setText(R.string.detalles_tabletop);

        // Se crea el bundle para pasar el tabletop como objeto hacia el fragmento
        Bundle bundle = new Bundle();
        bundle.putParcelable("tabletop", tabletopRecibido);

        // Se crea la instancia del fragmento, se le asignan los argumentos a pasar (tabletopRecibido por medio de bundle)
        // y se coloca el fragmento en el container designado para ello
        DetailsTabletopFragment detailsFragment = new DetailsTabletopFragment();
        detailsFragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.DetailsContainer, detailsFragment).commit();

    }
}