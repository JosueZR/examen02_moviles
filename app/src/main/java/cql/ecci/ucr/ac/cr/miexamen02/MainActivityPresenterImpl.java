package cql.ecci.ucr.ac.cr.miexamen02;

import android.content.Context;

import java.util.List;

import cql.ecci.ucr.ac.cr.miexamen02.Interfaces.MainActivityPresenter;
import cql.ecci.ucr.ac.cr.miexamen02.Interfaces.MainActivityView;
import cql.ecci.ucr.ac.cr.miexamen02.Interfaces.TabletopInteractor;

public class MainActivityPresenterImpl implements MainActivityPresenter, TabletopInteractor.OnFinishedListener {

    private MainActivityView mMainActivityView;
    private TabletopInteractor mTabletopInteractor;
    private Context context;

    // Constructor del Presenter
    public MainActivityPresenterImpl(MainActivityView mainActivityView, Context context) {
        this.mMainActivityView = mainActivityView;
        this.context = context;

        // Capa de negocios (Interactor)
        this.mTabletopInteractor = new TabletopInteractorImpl();
    }

    @Override public void onResume() {
        if (mMainActivityView != null) {
            //mMainActivityView.showProgress();
        }
        // Obtener los items de la capa de negocios (Interactor)
        mTabletopInteractor.getItems(this, context);
    }


    // Evento de clic en la lista
    @Override public void onItemClicked(int position) {
        if (mMainActivityView != null) {
            mMainActivityView.showMessage(String.format("Position %d clicked", position + 1));
        }
    }


    @Override public void onDestroy() {
        mMainActivityView = null;
    }

    @Override
    public void onFinished(List<Tabletop> items) {
        if (mMainActivityView != null) {

            mMainActivityView.setItems(items);

            mMainActivityView.hideProgress();
        }
    }

    // Retornar la vista
    public MainActivityView getMainView() {
        return mMainActivityView;
    }

}
