package cql.ecci.ucr.ac.cr.miexamen02;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import cql.ecci.ucr.ac.cr.miexamen02.Exceptions.DataItemsException;
import cql.ecci.ucr.ac.cr.miexamen02.Interfaces.IServiceDataSource;
import cql.ecci.ucr.ac.cr.miexamen02.Interfaces.TabletopInteractor;

public class ServiceDataSourceImpl implements IServiceDataSource {

    public List<Tabletop> items = null;
    private TaskServicioREST servicioREST;

    // Se define el listener para que se pueda dar la reacción cuando se complete
    // la carga de datos
    private TabletopInteractor.OnFinishedListener listener;

    // Se define el URL que contiene los datos a extraer
    private static final String JSON_URL = "https://bitbucket.org/lyonv/ci0161_i2020_examenii/raw/996c22731408d9123feac58627318a7859e82367/Tabletop19.json";

    /**
     * Constructor
     * @param listener que se usará para indicar cuando la tarea de carga haya sido completada
     */
    public ServiceDataSourceImpl(TabletopInteractor.OnFinishedListener listener)
    {
        this.listener = listener;
        this.servicioREST = new TaskServicioREST();
    }

    /**
     * Método que se encarga de invocar a la tarea asíncrona para extraer los datos y al mismo tiempo
     * tener la barra de progreso funcionando
     * @param context que es el contexto de la aplicación
     * @return items, que es una lista de elementos Tabletop
     * @throws DataItemsException
     */
    @Override
    public List<Tabletop> obtainItems(Context context) throws DataItemsException {

        items = new ArrayList<>();

        // Ejecución del servicio
        servicioREST.execute(JSON_URL);
        return items;
    }

    /**
     * Método que se encarga de extraer del contenido en formato .json que se encuentra
     * en el String datos, los tabletops que posteriormente se devolverán a la vista.
     * @param datos que es el string que contiene todos los datos ya jalados desde el url
     */
    public void setTabletops(String datos)
    {
        try
        {
            //Se obtiene el json desde el get del servicio
            JSONObject obj = new JSONObject(datos);
            JSONArray tabletopArray = obj.getJSONArray("miTabletop");

            //Ciclo sobre todos los elementos del arreglo
            for (int i = 0; i < tabletopArray.length(); i++)
            {
                //Se obtiene el objeto json de una posición específica
                JSONObject jsonObject = tabletopArray.getJSONObject(i);

                // Se crea un nuevo Tabletop a la que se le asignan los datos extraídos del objeto
                Tabletop tabletop = new Tabletop(jsonObject.getString("identificacion"), jsonObject.getString("nombre"),
                        jsonObject.getInt("year"), jsonObject.getString("publisher"));

                // Se añade el tabletop a la lista
                items.add(tabletop);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Una vez finalizado, se llama al listener
        listener.onFinished(items);
    }


    // Clase para la tarea asincronica de Gson en Servicio REST
    private class TaskServicioREST extends AsyncTask<String, Float, String> {

        // Se crea un dialog local para el progreso
        private ProgressDialog mDialog;
        private String datosTemp;

        /**
         * Constructor
         */
        public TaskServicioREST()
        {
            mDialog = MainActivity.getDialog();
        }

        /**
         * Método que se ejecutará antes de la ejecución de la tarea principal
         */
        @Override
        protected void onPreExecute()
        {
            mDialog.setProgress(0);
        }

        /**
         * Método que se encarga de lanzar en otro thread las tareas pertinentes
         * @param urls que en este caso, contiene la dirección donde se encuentran los
         *          datos almacenados
         * @return loadContentFromNetwork, que es el método que se ejecuta por detrás
         */
        @Override
        protected String doInBackground(String... urls) {
            for (int i = 0; i <= 100; i++) {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {}
                publishProgress(i/100f);
            }
            // tomanos el parámetro del execute() y bajamos el contenido
            return loadContentFromNetwork(urls[0]);
        }


        /**
         * Método que, luego de ejecutar la tarea de extracción, se encarga de
         * ocultar la barra de progreso y actualizar la lista de Tabletops
         * @param result que contiene el archivo json ya extraído en un String
         */
        protected void onPostExecute(String result)
        {
            mDialog.dismiss();
            setTabletops(datosTemp);
        }


        /**
         * Método utilizado dentro del task para actualizar la barra de progreso
         * @param values que son los valores que se usarán para ir avanzando con la barra de progreso
         */
        protected void onProgressUpdate(Float... values)
        {
            int p = Math.round(100 * values[0]);
            mDialog.setProgress(p);     // Se actualiza el número mostrado en la barra de progreso
        }


        /**
         * Método que se usa para cargar el contenido de la red, desde el url que servirá
         * como fuente de datos
         * @param url, la dirección donde se localiza el archivo a utilizar
         * @return los datos dentro de un String
         */
        private String loadContentFromNetwork(String url) {

            try
            {
                // Se crea el canal y se hace el llamado al url para obtener los datos
                InputStream mInputStream = (InputStream) new URL(url).getContent();
                InputStreamReader mInputStreamReader = new InputStreamReader(mInputStream);
                BufferedReader responseBuffer = new BufferedReader(mInputStreamReader);
                StringBuilder strBuilder = new StringBuilder();
                String line = null;
                while ((line = responseBuffer.readLine()) != null) {
                    strBuilder.append(line);
                }

                // Se le asigna a la variable datosTemp el string sacado del json
                datosTemp = strBuilder.toString();
                //System.out.println(datosTemp);
                return strBuilder.toString();

            } catch (Exception e) {
                Log.v("IMG", e.getMessage());
            }
            return null;
        }
    }
}
