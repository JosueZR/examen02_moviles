package cql.ecci.ucr.ac.cr.miexamen02;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailsTabletopFragment extends Fragment {

    // Se define el tabletop que se va a recibir
    Tabletop tabletopRecibido;

    // Se crea la lista de strings
    private List<String> mDetails;

    private ListView lista;

    public DetailsTabletopFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Se define el View para el fragmento
        View view = inflater.inflate(R.layout.fragment_details_tabletop, container, false);

        // Se recibe el Tabletop
        if (getArguments() != null)
            tabletopRecibido = getArguments().getParcelable("tabletop");
        else
            tabletopRecibido = getActivity().getIntent().getParcelableExtra("tabletop");

        lista = view.findViewById(R.id.listDetails);

        // Se instancia la lista y se llena
        mDetails = new ArrayList<>();
        llenarDetallesTabletop();

        // Se define el adaptador para la lista
        ArrayAdapter<String> adapter = new ArrayAdapter<>(view.getContext(),
                android.R.layout.simple_list_item_1, android.R.id.text1, mDetails);

        // Se asigna el adaptador al ListView
        lista.setAdapter(adapter);

        return view;
    }

    /**
     * Método que se encarga de llenar el String a desplegar en la lista con cada uno de los datos
     * pertinentes al Tabletop que se planea detallar
     */
    private void llenarDetallesTabletop() {

        // Se agregan los elementos extraídos del tabletop recibido en la actividad
        mDetails.add("ID: " + tabletopRecibido.getIdentificacion());
        mDetails.add("Nombre: " + tabletopRecibido.getNombre());
        mDetails.add("Publisher: " + tabletopRecibido.getPublisher());
        mDetails.add("Year: " + tabletopRecibido.getYear());
    }
}