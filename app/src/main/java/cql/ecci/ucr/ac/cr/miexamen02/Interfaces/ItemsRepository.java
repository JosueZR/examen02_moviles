package cql.ecci.ucr.ac.cr.miexamen02.Interfaces;

import android.content.Context;

import java.util.List;

import cql.ecci.ucr.ac.cr.miexamen02.Exceptions.CantRetrieveItemsException;
import cql.ecci.ucr.ac.cr.miexamen02.Tabletop;

public interface ItemsRepository {

    // Método desde el cual se llamará al servicio de datos
    List<Tabletop> obtainItems(Context context, TabletopInteractor.OnFinishedListener listener) throws CantRetrieveItemsException;
}
