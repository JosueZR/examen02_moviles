package cql.ecci.ucr.ac.cr.miexamen02.Interfaces;

import android.content.Context;

import java.util.List;

import cql.ecci.ucr.ac.cr.miexamen02.Tabletop;

public interface TabletopInteractor {

    // Interface que servirá para disparar acciones luego de realizar tareas
    interface OnFinishedListener {
        void onFinished(List<Tabletop> items);
    }

    void getItems(OnFinishedListener listener, Context context);
}
