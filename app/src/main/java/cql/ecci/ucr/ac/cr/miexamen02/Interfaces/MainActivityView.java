package cql.ecci.ucr.ac.cr.miexamen02.Interfaces;

import java.util.List;

import cql.ecci.ucr.ac.cr.miexamen02.Tabletop;

public interface MainActivityView {

    // Mostrar el progreso en la UI del avance de la tarea a realizar
    void showProgress();

    // Esconder el indicador de progreso de la UI
    void hideProgress();

    // Mostrar los items de la lista en la UI
    void setItems(List<Tabletop> items);

    // Mostrar mensaje en la UI
    void showMessage(String message);
}
