package cql.ecci.ucr.ac.cr.miexamen02.Interfaces;

import android.content.Context;

import java.util.List;

import cql.ecci.ucr.ac.cr.miexamen02.Exceptions.DataItemsException;
import cql.ecci.ucr.ac.cr.miexamen02.Tabletop;

public interface IServiceDataSource {

    // Método desde el cuál se obtendrán los items desde la fuente de datos
    List<Tabletop> obtainItems (Context context) throws DataItemsException;
}
