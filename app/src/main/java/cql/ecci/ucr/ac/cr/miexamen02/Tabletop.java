package cql.ecci.ucr.ac.cr.miexamen02;

import android.os.Parcel;
import android.os.Parcelable;

public class Tabletop implements Parcelable {

    // Definición de atributos de la clase
    private String identificacion;
    private String nombre;
    private Integer year;
    private String publisher;

    // Constructor
    public Tabletop (String identificacion, String nombre, Integer year, String publisher) {
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.year = year;
        this.publisher = publisher;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    // Método necesario para implementar la clase Parcelable
    protected Tabletop(Parcel in)
    {
        setIdentificacion(in.readString());
        setNombre(in.readString());
        setYear(in.readInt());
        setPublisher(in.readString());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getIdentificacion());
        dest.writeString(getNombre());
        dest.writeInt(getYear());
        dest.writeString(getPublisher());
    }

    public static final Creator<Tabletop> CREATOR = new Creator<Tabletop>() {
        @Override
        public Tabletop createFromParcel(Parcel in) {
            return new Tabletop(in);
        }

        @Override
        public Tabletop[] newArray(int size) {
            return new Tabletop[size];
        }
    };

}
