package cql.ecci.ucr.ac.cr.miexamen02;

import android.content.Context;

import java.util.List;

import cql.ecci.ucr.ac.cr.miexamen02.Exceptions.CantRetrieveItemsException;
import cql.ecci.ucr.ac.cr.miexamen02.Exceptions.DataItemsException;
import cql.ecci.ucr.ac.cr.miexamen02.Interfaces.IServiceDataSource;
import cql.ecci.ucr.ac.cr.miexamen02.Interfaces.ItemsRepository;
import cql.ecci.ucr.ac.cr.miexamen02.Interfaces.TabletopInteractor;

public class ItemsRepositoryImpl implements ItemsRepository {

    // Se instancia el servicio que se va a utilizar, por medio de su interface
    private IServiceDataSource mDataBaseDataSource;

    @Override
    public List<Tabletop> obtainItems(Context context, TabletopInteractor.OnFinishedListener listener) throws CantRetrieveItemsException {

        // Se inicializa la lista a devolver con null
        List<Tabletop> items = null;

        try {
            // Se instancia al servicio y se llama a su método obtainItems
            mDataBaseDataSource = new ServiceDataSourceImpl(listener);
            items = mDataBaseDataSource.obtainItems(context);

        } catch (DataItemsException e) {

            throw new CantRetrieveItemsException(e.getMessage());
        }

        return items;
    }
}
