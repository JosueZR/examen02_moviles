package cql.ecci.ucr.ac.cr.miexamen02;

import androidx.test.espresso.intent.rule.IntentsTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.intent.Intents.intended;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.anything;

@RunWith(AndroidJUnit4.class)
public class ListTableTopTest{

    @Rule
    public IntentsTestRule<MainActivity> mActivityRule = new IntentsTestRule<>(MainActivity.class);

    /**
     * Prueba que se encarga de comprobar si el listview que despliega todos
     * los tabletop
     * @throws Exception
     */
    @Test
    public void listViewPresente() throws Exception
    {
        onView(withId(R.id.list)).check(matches(isDisplayed()));
    }

    /**
     * Prueba que se encarga de comprobar que al seleccionar el primer elemento
     * de la lista se despliegue la actividad con su respectivo detalle
     * @throws Exception
     */
    @Test
    public void primerElementoDetalle() throws Exception
    {
        // Click al primer elemento del listview
        onData(anything()).inAdapterView(withId(R.id.list)).atPosition(0).perform(click());

        // Se chequea que se haya lanzado la actividad DetailsPersona
        intended(hasComponent(DetailsTabletopActivity.class.getName()));

        // Aquí ya está en la otra actividad (DetailsTabletop)
        onView(withId(R.id.textViewActivity)).check(matches(withText(R.string.detalles_tabletop)));
        onView(withId(R.id.listDetails)).check(matches(isDisplayed()));
    }

}
